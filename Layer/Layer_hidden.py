#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*


from Sigmoide.Sigmoide_hidden import Sigmoide_hidden


class Layer_hidden:

	def __init__(self, nb, back):
		' Constructor of the hidden layer '
		self.dimension = nb
		self.backLayer = back
		self.neuron = []
		for index in range(0, nb):
			self.neuron.append(Sigmoide_hidden(self.backLayer.dimensions()))


	def run(self):
		' Launch every neuron contains by layer '
		backResult = self.backLayer.entry()
		result = []
		for current in self.neuron:
			result.append(current.run(backResult))
		return result


	def out(self, input):
		' Display every input and corresponding weight of every neuron of the layer '
		for current in self.neuron:
			current.aff(input)


	def dimensions(self):
		' Number of neuron in the layer '
		return self.dimension
