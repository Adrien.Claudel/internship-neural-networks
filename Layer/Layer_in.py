#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

import random
from Sigmoide.Sigmoide_in import Sigmoide_in


class Layer_in:

	def __init__(self, nb):
		' Constructor of the input layer '
		self.dimension = nb
		self.neuron = []
		for index in range(0, nb):
			self.neuron.append(Sigmoide_in())


	def run(self, input):
		' Launch every neuron contains by layer '
		if len(input) == len(self.neuron):
			result = []
			for index in range(0, 784):
				result.append(self.neuron[index].run(input[index]))
		return result


	def out(self):
		' Display every input and corresponding weight of every neuron of the layer '
		for current in self.neuron:
			current.aff()


	def entry(self):
		' Randomly initialize all entries (necessary for debugging) '
		entry = []
		for index in range(0, 784):
			entry.append(random.random())
		return self.run(entry)


	def dimensions(self):
		' Number of neuron in the layer '
		return self.dimension
