#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

from Sigmoide.Sigmoide_out import Sigmoide_out


class Layer_out:

	def __init__(self, nb, back):
		' Constructor of the output layer '
		self.backLayer = back
		self.neuron = []
		for index in range(0, nb):
			self.neuron.append(Sigmoide_out(self.backLayer.dimensions()))


	def run(self):
		' Launch every neuron contains by layer '
		backResult = self.backLayer.run()
		result = []
		for current in self.neuron:
			result.append(current.run(backResult))
		return result


	def grad(self, expected):
		index = 0
		for current in self.neuron:
			print("______________________________________________", index ,"________________________________________________")
			current.grad(expected[0])
			del expected[0]
			index += 1


	def out(self, input):
		' Display every input and corresponding weight of every neuron of the layer '
		for current in self.neuron:
			current.aff(input)


	def initpod(self):
		for current in self.neuron:
			current.initpod()