#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

import math
import random


class Sigmoide_hidden:

	def __init__(self, nbEntry):
		' Constructor of a sigmoide for a hidden layer '
		self.pod = []
		self.bias = -1.0
		self.learning = 0.1
		for i in range(0, nbEntry):
			self.pod.append(random.random())


	def run(self, input):
		' Launch neurone '
		if len(self.pod) == len(input):
			self.input = input
			sum = 0.0
			for i in range(0, len(input)):
				sum = sum + self.pod[i] * input[i]
			result = 1.0 / 1.0 + math.exp(-sum -self.bias)
			return result
		else:
			print("Neurone hidden input error !")


	def grad(self, y, om):
		' Correction of the neuron weights according to the last result obtained '
		z = 1.0 / (1.0 + math.exp(-om))
		delta = (0.0 - z) * z * (1.0 - z)
		return self.learning * delta


	def om(self):
		' Calculates intermediate multiplying weight by their inputs necessary to the activation function '
		result = 0.0
		for i in range(0, len(self.input)):
			result = result + self.pod[i] * self.input[i]
		return result


	def aff(self, input):
		' Displaying the inputs of a neuron with their corresponding weights '
		for i in range(0, len(self.pod)):
			print("-", round(input[i], 2), "  \t-", round(self.pod[i], 2))