#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

import math
import random

class Sigmoide_in:

	def __init__(self):
		' Constructor of a intput sigmoide '
		self.bias = -1.0
		self.learning = 1.0
		self.pod = random.random()


	def run(self, input):
		' Launch neurone '
		sum = self.pod * input
		result = 1.0 / 1.0 + math.exp(-sum -self.bias)
		return result


	def grad(self, y):
		' Correction of the neuron weights according to the last result obtained '
		om = self.pod * self.input
		z = 1.0 / 1.0 + math.exp(-om -self.bias)
		delta = (y - z) * z * (1.0 - z)
		pod = pod + self.learning * delta


	def aff(self):
		' Displaying the inputs of a neuron with their corresponding weights '
		print("-", round(self.input, 2), "  \t-", round(self.pod, 2))
