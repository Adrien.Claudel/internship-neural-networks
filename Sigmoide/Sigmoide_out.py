#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

import math
import random


class Sigmoide_out:

	def __init__(self, nbEntry):
		' Constructor of a output sigmoide '
		self.pod = []
		self.entry = []
		self.bias = -1.0
		self.learning = 0.1
		self.dimensions = nbEntry


	def run(self, input):
		' Launch neurone '
		self.entry = input
		if len(self.pod) == len(input):
			result = 1.0 / 1.0 + math.exp(-self.om(input) -self.bias)
			return result
		else:
			print("Neurone out input error !")


	def grad(self, expected):
		' Correction of the neuron weights according to the last result obtained '
		z = 1.0 / (1.0 + math.exp(-self.om(self.entry)))
		indexx = 0

		print()
		print("wn\t\tlearning", "\t\t\tdelta\t\t\t\tw", indexx, "\t\t\t\t\t new wn")

		for current in self.pod:
			delta = (expected - z) * z * (1.0 - z)
			print("w", indexx, "\t", self.learning, " * ", delta, " * ", current, " =\t", end='')
			self.pod[indexx] = self.learning * delta * current
			print(self.pod[indexx])
			indexx += 1


	def om(self, input):
		' Calculates intermediate multiplying weight by their inputs necessary to the activation function '
		result = 0.0
		for i in range(0, len(input)):
			result = result + self.pod[i] * input[i]
		return result


	def initpod(self):
		' Initialize randomly all the weight '
		for i in range(0, self.dimensions):
			self.pod.append(random.random())


	def aff(self, input):
		' Displaying the inputs of a neuron with their corresponding weights '
		for i in range(0, len(self.pod)):
			print("-", round(input[i], 2), "  \t-", round(self.pod[i], 2))