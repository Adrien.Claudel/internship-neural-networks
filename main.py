#!/usr/bin/python3.6.5
# -*-coding:Utf-8 -*

import math
import random

from Sigmoide.Sigmoide_in import Sigmoide_in
from Sigmoide.Sigmoide_hidden import Sigmoide_hidden
from Sigmoide.Sigmoide_out import Sigmoide_out

from Layer.Layer_in import Layer_in
from Layer.Layer_hidden import Layer_hidden
from Layer.Layer_out import Layer_out



def process(input):
	""" Display each element of a table """
	for current in enumerate(input):
		print(current)


#---------------------------------------------------------------#

l_input = Layer_in(784)		# Initialize the input layer
result1 = l_input.entry()	# Give the input and launch
#process(result1)			# Display result

#---------------------------------------------------------------#

l_hidden = Layer_hidden(15, l_input)	# Initialize the hidden layer
result2 = l_hidden.run()		# Give the input from the input layer and launch
#process(result2)			# Display result

#---------------------------------------------------------------#

l_out = Layer_out(10, l_hidden)		# Initialize the output layer
#out_l.initpod()
#result3 = l_out.run()			# Give the input from the hidden layer and launch
#process(result3)			# Display result
	
#---------------------------------------------------------------#

l_out.initpod()
for i in range(0, 10):
	expected = [0] * 10
	rn = int(random.random() * 10)
	expected[rn] = 1
	print("\n\n_______try:", i, "_______expected:", rn, "_______result:", l_out.run().index(max(l_out.run())), "_______")
	process(l_out.run())
	l_out.grad(expected)